# BeeScreens Node Library
[![Pipeline Status][pipeline-status-badge]][pipeline-status-url]
[![Coverage Report][coverage-report-badge]][coverage-report-url]
[![License][license-badge]][license-url]
[![Code of Conduct][code-of-conduct-badge]][code-of-conduct-url]
[![Keep a Changelog v1.1.0 badge][changelog-badge]][changelog-url]
[![BeeScreens' Telegram Channel][telegram-channel-badge]][telegram-channel-url]
[![BeeScreens' Telegram Community][telegram-community-badge]][telegram-community-url]

## Introduction

BeeScreens is a framework allowing any third-party developpers to develop new interactive applications to be streamed over the Internet from one media (i.e. a phone, computer, etc) to any other media both only using a modern Web Browser. See the [BeeScreens official website][beescreens-url] for more details on how BeeScreens works!

## Use BeeScreens Node Library

You must be in your `src` folder, next to your `package.json` for these instructions to work.

### Add access to the GitLab NPM registry

```sh
# Create the NPM configuration fle and add the GitLab NPM registry
echo '@beescreens:registry=https://gitlab.com/api/v4/packages/npm/' > .npmrc
```

### Install the BeeScreens Node Library package

```sh
npm install --save @beescreens/beescreens
```

### Use the BeeScreens Node Library package

In your application, use the BeeScreens Node Library package as any other packages:

```sh
# Example
import { Application } from '@beescreens/beescreens';
```
## Technologies used

The BeeScreens Node Library uses the following technologies:

- [TypeScript](https://www.typescriptlang.org/) - TypeScript is a typed superset of JavaScript that compiles to plain JavaScript

## Changelog

Changelog can be found in the [CHANGELOG.md][changelog-url] file.

## License

This project is free and will always be. The source code is licensed under the MIT License - see the [LICENSE.md][license-url]
file for details.

## Contributing

Thank you for considering contributing to BeeScreens! Here is some help to get you started to contribute to the project:

1. Please start by reading our code of conduct available in the [CODE_OF_CONDUCT.md][code-of-conduct-url] file.
2. All contribution information is available in the [CONTRIBUTING.md][contributor-url] file.

Feel free to contribute to the project in any way that you can think of, your contributions are more than welcome!

## Want to reach us?

BeeScreens has the following main channels to communicate:

- [GitLab][repository-url], using [issues][issue-url]
- [BeeScreens' Telegram Channel][telegram-channel-url], where updates are sent when someone is working on BeeScreens
- [BeeScreens' Telegram Community][telegram-community-url], group where discussions are allowed to ask anything related to BeeScreens

Feel free to use any of the communication channels to reach us!

[pipeline-status-badge]: https://gitlab.com/beescreens/libraries/beescreens-node-library/badges/master/pipeline.svg
[pipeline-status-url]: https://gitlab.com/beescreens/libraries/beescreens-node-library/commits/master

[coverage-report-badge]: https://gitlab.com/beescreens/libraries/beescreens-node-library/badges/master/coverage.svg
[coverage-report-url]: https://gitlab.com/beescreens/libraries/beescreens-node-library/commits/master

[license-badge]: https://img.shields.io/badge/license-MIT-blue.svg
[license-url]: https://gitlab.com/beescreens/libraries/beescreens-node-library/blob/master/LICENSE.md

[code-of-conduct-badge]: https://img.shields.io/badge/code%20of%20conduct%20-Contributor%20Covenant%20v2.0-ff69b4.svg
[code-of-conduct-url]: https://gitlab.com/beescreens/libraries/beescreens-node-library/blob/master/CODE_OF_CONDUCT.md

[contributor-url]: https://gitlab.com/beescreens/libraries/beescreens-node-library/blob/master/CONTRIBUTING.md

[changelog-badge]: https://img.shields.io/badge/changelog-Keep%20a%20Changelog%20v1.1.0-%23E05735.svg
[changelog-url]: https://gitlab.com/beescreens/libraries/beescreens-node-library/blob/master/CHANGELOG.md

[telegram-channel-badge]: https://img.shields.io/badge/telegram-BeeScreens%20Channel-blue.svg
[telegram-channel-url]: https://t.me/beescreens

[telegram-community-badge]: https://img.shields.io/badge/telegram-BeeScreens%20Community-blue.svg
[telegram-community-url]: https://t.me/beescreens_community

[beescreens-url]: https://beescreens.ch
[docker-files-website]: https://docs.docker.beescreens.ch

[repository-url]: https://gitlab.com/beescreens/libraries/beescreens-node-library
[issue-url]: https://gitlab.com/beescreens/libraries/beescreens-node-library/blob/master/ISSUE_TEMPLATE.md
