module.exports = {
  moduleFileExtensions: ['js', 'json', 'ts'],
  testEnvironment: 'node',
  testRegex: '((\\.|/)(test|spec))\\.[jt]sx?$',
  transform: {
    '^.+\\.(t|j)s$': 'ts-jest',
  },
};
