export enum WsQueueEvents {
  JOIN_AS_APPLICATION = 'join-as-application',
  JOIN_AS_PLAYER = 'join-as-player',
  QUEUE_POSITION = 'queue-position',
  QUEUE_NEXT = 'queue-next',
}
