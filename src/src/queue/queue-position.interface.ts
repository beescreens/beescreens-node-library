export interface QueuePosition {
  readonly position: number;
  readonly total: number;
}
