export enum PlayerStatus {
  CREATED = 'created',
  IN_QUEUE = 'in-queue',
  PLAYING = 'playing',
  QUIT = 'quit',
  KICKED = 'kicked',
}
