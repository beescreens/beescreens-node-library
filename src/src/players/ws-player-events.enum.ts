export enum WsPlayerEvents {
  JOIN_AS_USER = 'join-as-user',
  JOIN_AS_PLAYER = 'join-as-player',
  PLAYER_CREATED = 'player-created',
  PLAYER_UPDATED = 'player-updated',
}
