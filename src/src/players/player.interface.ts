import { PlayerStatus } from './player-status.enum';

export interface Player {
  readonly id: string;
  readonly status: PlayerStatus;
  readonly createdAt: Date;
  readonly updatedAt: Date;
}
