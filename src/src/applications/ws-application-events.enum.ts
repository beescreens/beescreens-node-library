export enum WsApplicationEvents {
  JOIN_AS_USER = 'join-as-user',
  JOIN_AS_APPLICATION = 'join-as-application',
  APPLICATION_CREATED = 'application-created',
  APPLICATION_UPDATED = 'application-updated',
  APPLICATION_DELETED = 'application-deleted',
}
