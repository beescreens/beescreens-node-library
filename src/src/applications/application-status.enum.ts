export enum ApplicationStatus {
  ENABLED = 'enabled',
  DISABLED = 'disabled',
}
