import { ApplicationTier } from './application-tier.emum';
import { ApplicationStatus } from './application-status.enum';
import { ApplicationRole } from './application-role.emum';

export interface Application {
  readonly id: string;
  readonly apiKey: string;
  readonly name: string;
  readonly description: string;
  readonly version: string;
  readonly contact: string;
  readonly timeLimit: number;
  readonly logoUrl: string;
  readonly homepageUrl: string;
  readonly documentationUrl: string;
  readonly endpointUrl: string;
  readonly minimumNumberOfPlayers: number;
  readonly maximumNumberOfPlayers: number;
  readonly minimumNumberOfScreens: number;
  readonly maximumNumberOfScreens: number;
  readonly tier: ApplicationTier;
  readonly role: ApplicationRole;
  readonly status: ApplicationStatus;
  readonly createdAt: Date;
  readonly updatedAt: Date;
}
