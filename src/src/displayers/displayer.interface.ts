import { DisplayerStatus } from './displayer-status.enum';
import { DisplayerRole } from './displayer-role.emum';

export interface Displayer {
  readonly id: string;
  readonly apiKey: string;
  readonly name: string;
  readonly location: string;
  readonly width: number;
  readonly height: number;
  readonly role: DisplayerRole;
  readonly status: DisplayerStatus;
  readonly createdAt: Date;
  readonly updatedAt: Date;
}
