export enum WsDisplayerEvents {
  JOIN_AS_USER = 'join-as-user',
  JOIN_AS_DISPLAYER = 'join-as-displayer',
  DISPLAYER_CREATED = 'displayer-created',
  DISPLAYER_UPDATED = 'displayer-updated',
  DISPLAYER_DELETED = 'displayer-deleted',
  UPDATED_DISPLAYER_RESOLUTION = 'updated-displayer-resolution',
}
