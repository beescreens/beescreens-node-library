export enum DisplayerStatus {
  ENABLED = 'enabled',
  DISABLED = 'disabled',
}
