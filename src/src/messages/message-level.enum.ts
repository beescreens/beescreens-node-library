export enum MessageLevel {
  INFO = 'info',
  WARNING = 'warning',
  ERROR = 'error',
}
