import { MessageLevel } from './message-level.enum';
import { MessageType } from './message-type.enum';

export interface Message {
  readonly title: string;
  readonly content: string;
  readonly level: MessageLevel;
  readonly type: MessageType;
}
