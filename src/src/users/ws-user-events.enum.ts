export enum WsUserEvents {
  JOIN_AS_USER = 'join-as-user',
  USER_CREATED = 'user-created',
  USER_UPDATED = 'user-updated',
  USER_DELETED = 'user-deleted',
}
