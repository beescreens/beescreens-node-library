export enum UserRole {
  ADMIN = 'admin',
  MODERATOR = 'moderator',
  DEVELOPER = 'developer',
  READ_ONLY = 'read-only',
}
