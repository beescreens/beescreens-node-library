import { UserRole } from './user-role.emum';
import { UserStatus } from './user-status.enum';

export interface User {
  readonly id: string;
  readonly username: string;
  readonly password: string;
  readonly role: UserRole;
  readonly status: UserStatus;
  readonly createdAt: Date;
  readonly updatedAt: Date;
}
