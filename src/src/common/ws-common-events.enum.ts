export enum WsCommonEvents {
  CONNECT = 'connect',
  DISCONNECT = 'disconnect',
  EXCEPTION = 'exception',
  JOINED = 'joined',
  PING = 'ping',
  PONG = 'pong',
}
