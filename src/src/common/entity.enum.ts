export enum Entity {
  APPLICATION = 'application',
  DISPLAYER = 'displayer',
  PLAYER = 'player',
}
