🐝: "*Bzzz you have requested the issue template?*"

- *You have any question regarding contribution? Please see the [CONTRIBUTING.md][contributor-url] file.*
- *Are you opening an issue regarding a bug? Please use the [Bug.md][bug-issue-url] template.*
- *Are you opening an issue regarding documentation? Please use the [Documentation.md][documentation-issue-url] template.*
- *Are you opening an issue regarding a feature request? Please use the [Feature request.md][feature-request-issue-url] template.*
- *None of the template above fits your need? Please use the [Other.md][other-issue-url] template.*

*Thanks for making BeeScreens better!*

[contributor-url]: https://gitlab.com/beescreens/libraries/beescreens-node-library/blob/master/CONTRIBUTING.md
[bug-issue-url]: https://gitlab.com/beescreens/libraries/beescreens-node-library/blob/master/.gitlab/issue_templates/Bug.md]
[documentation-issue-url]: https://gitlab.com/beescreens/libraries/beescreens-node-library/blob/master/.gitlab/issue_templates/Documentation.md]
[feature-request-issue-url]: https://gitlab.com/beescreens/libraries/beescreens-node-library/blob/master/.gitlab/issue_templates/Feature%20request.md]
[other-issue-url]: https://gitlab.com/beescreens/libraries/beescreens-node-library/blob/master/.gitlab/issue_templates/Other.md]
