# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.0.0] - 2020-05-22

### Added

- Set up a complete pipeline regarding the player:
  - Define the application player
  - Define the WebSockets events

### Changed

- `JOIN_AS_INTERNAL_ENTITY` and `JOIN_AS_EXTERNAL_ENTITY` are replaced with `JOIN_AS_*` for each entity.
- Changed events from `created-*` to `*-created`
- Changed events from `updated-*` to `*-updated`
- Changed events from `deleted-*` to `*-deleted`

## [1.1.0] - 2020-05-14

- Set up a complete pipeline regarding the application:
  - Define the application interface
  - Define the WebSockets events

## [1.0.0] - 2020-05-13

### Added

- Set up a complete pipeline regarding the displayer:
  - Define the displayer interface
  - Define the WebSockets events
- WebSockets events `PING` and `PONG`

### Changed

- WebSockets events `AUTHENTICATE` and `AUTHENTICATED` are replaced with `JOIN_AS_INTERNAL_ENTITY`, `JOIN_AS_EXTERNAL_ENTITY` and `JOINED`

## [0.0.2] - 2020-04-17

### Added

- Set up a complete pipeline regarding the API:
  - Define the user interface
  - Define the message interface
  - Define the WebSockets events

## [0.0.1] - 2020-02-26

### Added

- Set up a complete pipeline with best pratices regarding:
  - Documentation
  - Contributions files
  - CI/CD
  - Unit testing
  - Building packages
  - Auditing
  - Linting
  - Scripts helpers

[2.0.0]: https://gitlab.com/beescreens/libraries/beescreens-node-library/-/tags/2.0.0
[1.1.0]: https://gitlab.com/beescreens/libraries/beescreens-node-library/-/tags/1.1.0
[1.0.0]: https://gitlab.com/beescreens/libraries/beescreens-node-library/-/tags/1.0.0
[0.0.2]: https://gitlab.com/beescreens/libraries/beescreens-node-library/-/tags/0.0.2
[0.0.1]: https://gitlab.com/beescreens/libraries/beescreens-node-library/-/tags/0.0.1